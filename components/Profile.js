import React, { useState } from 'react';
import { BleManager } from 'react-native-ble-manager';
import { MaskedTextInput } from "react-native-mask-text";
import {
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    Switch,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity
} from 'react-native';
import BLService from '../redux/actions';

export default function Profile({ route, navigation, logMessage, getLocation }) {
    const [isEnabled, setIsEnabled] = useState(false);
    const BLEManager = new BleManager();
    const toggleSwitch = () => {
        if (!isEnabled) {
            bluetoothScan();
        }
        setIsEnabled(previousState => !previousState);
    };

    const bluetoothScan = () => {
        logMessage('Начинаем сканировать устройства поблизости..');
        BLEManager.startDeviceScan(null, {}, (error, scannedDevice) => {
          logMessage(`Обнаружено устройство: ${scannedDevice.id}`)
          logMessage(`Отправляем на сервер ${scannedDevice.id}`)
    
          if (!error) {
            BLService.getIsMacRequested(scannedDevice.id).then((isRequested) => {
              if (isRequested) {
                logMessage(`Запрашиваем координаты устройства с UUID ${scannedDevice.id}`)
                getLocation().then((location) => {
                  logMessage(`Сохраняем координаты устройства с UUID ${scannedDevice.id} на сервер`)
                  BLService.saveUserLoactionByMac({
                    mac: scannedDevice.id,
                    ...location
                  });
                });
              }
            })
          }
        });
      }

    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.container}>
                <Image style={styles.image} source={require('../assets/Logo.png')} />
                <View style={styles.inputContainer}>
                    <Text>ФИО</Text>
                    <TextInput
                        style={styles.input}
                        placeholder='Введите ФИО'
                        defaultValue='Анатолий Анатольевич Анатоль'
                        onChangeText={() => {}}></TextInput>
                </View>
                <View style={styles.inputContainer}>
                    <Text>Телефон</Text>
                    <MaskedTextInput
                        keyboardType={'phone-pad'}
                        placeholder='+7 ('
                        defaultValue={route.params.phone}
                        style={styles.input}
                        onChangeText={() => {}}
                        mask="+7 (999) 999 99 99"></MaskedTextInput>
                </View>
                <View style={styles.bluetooth}>
                    <View style={styles.bluetoothHeader}>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={isEnabled}
                        />
                        <Text style={styles.bluetoothTitle}>Bluetooth</Text>
                    </View>
                    <Text style={styles.bluetoothSubtitle}>
                        Функция позволяет получить позицию при помощи соседних устройств
                    </Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => { navigation.navigate('Console', { isBluetoothEnabled: isEnabled }); }}>
                    <Text style={{ color: "white", fontSize: 16 }}>Открыть консоль</Text>
                </TouchableOpacity>

                <View style={styles.footer}>
                    <Text>UserID: { BLService.getUserId() }</Text>
                    <Text>UUID: { BLService.getUUID() }</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      flexDirection: "column",
      alignItems: "center",
    },
    image: {
      marginTop: 30,
      width: 100,
      height: 100,
      resizeMode: "contain",
    },
    inputContainer: {
        marginTop: 40,
        width: 330
    },
    input: {
      marginTop: 8,
      height: 40,
      width: '100%',
      borderWidth: 1,
      padding: 10,
      borderRadius: 8,
    },
    bluetooth: {
        marginTop: 30,
        padding: 10,
        paddingLeft: 0,
        paddingRight: 0,
        width: 330,
        flexGrow: 1
    },
    bluetoothHeader: {
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    bluetoothTitle: {
        letterSpacing: 0.25,
        lineHeight: 18,
        fontSize: 20,
        paddingLeft: 10
    },
    bluetoothSubtitle: {
        height: 28,
        display: 'flex',
        alignItems: 'flex-start',
        minWidth: 256,
        marginTop: 12,
        width: 340,
        color: "#999999",
        flex: 1,
        flexWrap: 'wrap'
    },
    button: {
        marginBottom: 130,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#B6BAC3",
        padding: 10,
        width: 340,
        borderRadius: 8,
        height: 44,
      },
      footer: {
          paddingBottom: 100
      }
  });