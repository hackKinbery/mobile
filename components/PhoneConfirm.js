import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity, TextInput
} from "react-native";

const MAX_FRAMES_AMOUNT = 3;

export default function PhoneConfirm({ navigation, route }) {
    const [frameNumber, setFrameNumber] = useState(0);

    useEffect(() => {
        if (frameNumber <= MAX_FRAMES_AMOUNT) {
            setTimeout(() => {
                setFrameNumber(frameNumber + 1);
            }, 700);
        } else {
            navigation.navigate('Profile', { phone: route.params.phone })
        }
    });

  return (
    <View style={styles.container}>    
      <Text style={styles.heading}>Введите код из сообщения</Text>
      <Text style={styles.subtitle}>
        SMS-код отправлен на номер
      </Text>
      <Text style={styles.subtitle}>
        +7 (999) 999 99-99
      </Text>
      {
          frameNumber <= MAX_FRAMES_AMOUNT && (
            <Image style={styles.image} source={
                frameNumber === 0 ? require('../assets/Frame4.png') :
                frameNumber === 1 ? require('../assets/Frame3.png') :
                frameNumber === 2 ? require('../assets/Frame2.png') :
                                    require('../assets/Frame1.png')
                } />
          )
      }
      <TextInput
        placeholder=""
        autoFocus={true}
        caretHidden={true}
        underlineColorAndroid='transparent'
        style={styles.TextInputStyle}
        keyboardType={'numeric'}/>
      <TouchableOpacity style={styles.button}>
        <Text style={{ color: "#999999", fontSize: 14 }}>Получить код повторно</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center",
  },
  image: {
    marginTop: 34,
    width: 252,
    height: 16,
    resizeMode: "cover",
  },
  heading: {
    marginTop: 64,
    fontSize: 18,
    fontWeight: "600",
    letterSpacing: 0.1,
  },
  subtitle: {
    marginTop: 8,
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0.4,
    color: "#999999",
    textAlign: "center",
    width: 300,
  },
  button: {
    marginTop: 34,
    alignItems: "center",
    justifyContent: "center",    
    width: 340,
    borderRadius: 8,
    height: 44,
  }, 
});