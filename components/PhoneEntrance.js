import { MaskedTextInput } from "react-native-mask-text";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";

export default function PhoneEntrance({ navigation }) {
    const [phone, setPhone] = useState(null);
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
            <Image style={styles.image} source={require("./../assets/image1.png")} />
            <Text style={styles.heading}>Войти или создать профиль</Text>
            <Text style={styles.subtitle}>
                Введите номер телефона для доступа к личному кабинету
            </Text>
            <MaskedTextInput
                keyboardType={'phone-pad'}
                style={styles.input}
                onChangeText={(text) => setPhone(text)}
                placeholder='+7 ('
                mask="+7 (999) 999 99 99"></MaskedTextInput>
            <TouchableOpacity style={styles.button} onPress={() => {
                navigation.navigate('PhoneConfirm', { phone });
            }}>
                <Text style={{ color: "white", fontSize: 16 }}>Продолжить</Text>
            </TouchableOpacity>
            <Text style={styles.underscore}>
                <Text>Нажимая на кнопку я соглашаюсь с условиями </Text>
                <Text style={{ textDecorationLine: "underline" }}>
                публичной оферты и политикой обработки персональных данных
                </Text>
            </Text>
        </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "column",
    alignItems: "center",
  },
  image: {
    marginTop: 68,
    width: 252,
    height: 200,
    resizeMode: "contain",
  },
  heading: {
    marginTop: 60,
    fontSize: 18,
    fontWeight: "600",
    letterSpacing: 0.1,
  },
  subtitle: {
    marginTop: 5,
    fontSize: 14,
    fontWeight: "400",
    letterSpacing: 0.4,
    color: "#999999",
    textAlign: "center",
    width: 300,
  },
  input: {
    marginTop: 16,
    height: 44,
    width: 340,
    borderWidth: 1,
    padding: 10,
    borderRadius: 8,
  },
  button: {
    marginTop: 8,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#B6BAC3",
    padding: 10,
    width: 340,
    borderRadius: 8,
    height: 44,
  },
  underscore: {
    marginTop: 12,
    fontSize: 11,
    width: 340,
    lineHeight: 18,
    color: "#999999",
  },
});
