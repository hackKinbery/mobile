import React, { useRef } from 'react';

import {
    View, ScrollView, Text, StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
    container: {
      borderWidth: 15,
      borderColor: '#E8E8E8',
      backgroundColor: '#E8E8E8',
      width: '100%',
      paddingBottom: 30
    }
  });

export default function Console({ log }) {
    const scrollView = useRef(null);

    return (
        <View style={{ height: '85%', marginTop: '10%' }}>
            <ScrollView
                style={styles.container}
                scrollEnabled={true}
                ref={scrollView}
                onContentSizeChange={() => scrollView.current.scrollToEnd({animated: true})}>
                {
                    log.map((message, index) => {
                        return <Text key={index} style={{ paddingTop: 15 }}>{ message }</Text>
                    })
                }
            </ScrollView>
        </View>
    )
}