import React, { useEffect, useState, useReducer, useRef } from 'react';
import { StyleSheet, Text, ScrollView, View } from 'react-native';
import * as Battery from 'expo-battery';
import * as Location from 'expo-location';
import * as Contacts from 'expo-contacts';
import * as Network from 'expo-network';

import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import PhoneConfirm from './components/PhoneConfirm';
import PhoneEntrance from './components/PhoneEntrance';
import Profile from './components/Profile';
import Console from './components/Console';

import BLService from './redux/actions';
import { rootReducer } from './redux/reducer';
import * as LZString from 'lz-string';
global.Buffer = global.Buffer || require('buffer').Buffer

const Stack = createNativeStackNavigator();

const store = createStore(
  rootReducer, compose(
    applyMiddleware(thunk)
  )
);

const styles = StyleSheet.create({
  container: {
    borderWidth: 15,
    borderColor: '#E8E8E8',
    backgroundColor: '#E8E8E8',
    width: '100%',
    paddingBottom: 30
  }
});

const INTERVAL_SELF_POSITIONING = 60000;
const INTERVAL_LOCATION_REQUEST_CHECK = 30000;
const PASSIVE_MODE_BATTERY_LEVEL = 0.3;

export default function App() {
  const [checkLocationInterval, setLocationInterval] = useState(INTERVAL_SELF_POSITIONING);
  const [locationMode, setLocationMode] = useState(null);
  const [locationPermission, setLocationPermission] = useState(null);
  const [log, appendLog] = useReducer((state, newMessage) => [...state, newMessage], [])

  const logMessage = (message) => {
    const m = new Date();
    const dateString =
        m.getUTCFullYear() + "/" +
        ("0" + (m.getUTCMonth()+1)).slice(-2) + "/" +
        ("0" + m.getUTCDate()).slice(-2) + " " +
        ("0" + m.getUTCHours()).slice(-2) + ":" +
        ("0" + m.getUTCMinutes()).slice(-2) + ":" +
        ("0" + m.getUTCSeconds()).slice(-2) + '.' +
        ("0" + m.getUTCMilliseconds()).slice(-2);

    appendLog(`${dateString}   ${message}`);
  }

  const runInterval = (processRequest, intervalTime) => {
    if (checkLocationInterval) {
      clearInterval(checkLocationInterval);
    }

    setLocationInterval(setInterval(processRequest, intervalTime));
  }

  /**
   * Если уровень заряда батареи не соответствует режиму
   *  сохранения коор-нат, меняем режим
   * @returns Уровень заряда батареи
   */
  const checkBattery = () => {
    return Battery.getBatteryLevelAsync().then(batteryLevel => {
      if (batteryLevel <= PASSIVE_MODE_BATTERY_LEVEL && locationMode === 'active' ||
          batteryLevel > PASSIVE_MODE_BATTERY_LEVEL && locationMode === 'passive'
      ) {
        if (locationMode === 'active') {
          logMessage(`Заряд баттареи ниже ${PASSIVE_MODE_BATTERY_LEVEL * 100}%. Переходим в пассивный режим определения геолокации`);
        } else {
          logMessage(`Заряд баттареи  ${PASSIVE_MODE_BATTERY_LEVEL * 100}%. Переходим в активный режим определения геолокации`);
        }
        setLocationMode(locationMode === 'active' ? 'passive' : 'active');
        return Promise.reject();
      }

      return Math.round(batteryLevel * 100);
    });
  }

  // Проверяет/получает разрешение на определение GPS-координат в фоне
  const getPermissionsForLocation = () => {
    Location.getBackgroundPermissionsAsync().then((permission) => {
      if (permission.status !== Location.PermissionStatus.GRANTED && permission.canAskAgain) {
        logMessage('Запрашиваем права на определение геолокации пользователя..')
        return Location.requestPermissionsAsync().then((requestedPermission) => {
          setLocationPermission(requestedPermission);
        });
      }

      setLocationPermission(permission);
    });
  }

  // Получение текущих коор-нат
  const getLocation = () => {
    if (locationPermission && locationPermission.granted) {
      return Location.getCurrentPositionAsync().then(({ coords: { latitude: lat, longitude: long } }) => ({ lat, long }))
    }
    return Promise.reject();
  }
  // Сохранение текущих коор-нат и переданного уровня заряда батареи
  const saveUserLocation = (battery) => {
    logMessage('Запрашиваем координаты пользователя...');
    return getLocation().then((locationData) => {
      logMessage(`Соохраняем координаты пользователя (${locationData.lat}, ${locationData.long}) на сервер`);
      BLService.saveUserLocation({
        ...locationData,
        battery
      });
    });
  }

  const init = () => {
    logMessage('Стартуем приложение...')

    Network.getMacAddressAsync()
      .then(BLService.setUserId)
      .then(getPermissionsForLocation);
  }

  useEffect(() => {
    if (locationPermission && locationPermission.granted) {
      setLocationMode('active');
    }
  }, [locationPermission]);

  useEffect(() => {
    if (locationMode === 'active') {
      runInterval(() => {
        checkBattery().then(saveUserLocation);
      }, INTERVAL_SELF_POSITIONING);
    } else {
      runInterval(() => {
        checkBattery().then((battery) => {
          logMessage('Определяем, есть ли запрос на наше местоположение..')
          BLService.getIsLocationRequested().then(isRequested => {
            if (isRequested) {
              saveUserLocation(battery);
            } else {
              logMessage('Запроса не было');
            }
          })
        });
      }, INTERVAL_LOCATION_REQUEST_CHECK);
    }
  }, [locationMode]);

  useEffect(() => {
    init();

    return () => {
      if (checkLocationInterval) {
        clearInterval(checkLocationInterval);
      }
    }
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="PhoneEntrance" component={PhoneEntrance} options={{ title: '' }}/>
        <Stack.Screen name="PhoneConfirm" component={PhoneConfirm} options={{ title: '' }} />
        <Stack.Screen name="Profile" options={{ title: 'Личный кабинет' }}>
        {(props) => <Profile {...props} getLocation={getLocation} logMessage={logMessage} />}
        </Stack.Screen>
        <Stack.Screen name="Console" options={{ title: 'Консоль' }}>
          {() => <Console log={log} logMessage={logMessage} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}