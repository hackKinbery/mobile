import { combineReducers } from 'redux';

const initialState = {}

export const commonReducer = (state = initialState, action) => {
    return state;
}

export const rootReducer = combineReducers({
    common: commonReducer
});
