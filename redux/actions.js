import axios from 'axios';

const baseUrl = 'http://104.248.102.232:8000';
let userId = null
let UUID = null

export default {
    // ******************* Bl Service *****************//
    saveUserLocation: (data) => axios.post(`${baseUrl}/api/location/saveUserLocation`, {
        userId,
        ...data
    }).then(res => res.data).catch(console.error),

    getIsLocationRequested: () => axios.get(`${baseUrl}/api/request/getRequestsByUserId/${userId}`)
        .then(res => Boolean(res.data))
        .catch(console.error),

    setUserId: (macAddress) => {
        UUID = macAddress;
        return axios.get(`${baseUrl}/api/userData/getUserIdByMac/${macAddress}`).then((res) => {
            userId = res.data.user_id;
        }).catch(console.error);
    },
    getUserId: () => userId,
    getUUID: () => 'ST1CUYOB-B8DB-IVBG-RBP3-EN1YV5GHLTNZ',

    getIsMacRequested: (macAddress) => axios.get(`${baseUrl}/api/bluetooth/getBluetoothRequest/${macAddress}`).then((res) => res.data).catch(console.error),

    saveUserLoactionByMac: (data) => axios.post(`${baseUrl}/api/location/saveUserLocationByMac`, data).then(res => res.data).catch(console.error)
}